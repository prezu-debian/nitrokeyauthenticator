/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef TOTPURIPARSER_H
#define TOTPURIPARSER_H

#include <QString>

/**
 * @brief The TotpUriParser class extracting account name,
 * secret and issuer from Uri
 */
class TotpUriParser
{
public:

    /**
     * @brief Constructs an instance of TotpUriParser.
     * @param original string.
     */
    TotpUriParser(const QString &original);

    /**
     * @brief getAccountName Returns account name extracted from the URI.
     * @return Account name.
     */
    QString getAccountName() const;

    /**
     * @brief Returns secret extracted from the URI.
     * @return The secret.
     */
    QString getSecret() const;

    /**
     * @brief Returns the issuer extracted from the URI.
     * @return The issuer.
     */
    QString getIssuer() const;

private:
    QString user;
    QString secret;
    QString issuer;
    QString query;

    void validateUrl(QString);
    void extractQuery(QString);
    void extractAccountName(QString);
    void validateQuery();
    void extractQueryValues();
};

#endif // TOTPURIPARSER_H
